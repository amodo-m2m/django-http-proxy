Django HTTP Proxy
=================

**NOTE: DEPRECATED: previously used for Sencha and frontend development. **

**NOTE: This project has moved from [Bitbucket][1] to [Github][2]**.

---

Thanks for downloading Django HTTP Proxy!

Documention is provided in Sphinx format in the `docs` subdirectory. To
build the HTML version of the documentation yourself, run:

    $ cd docs
    $ make html

Alternatively, you can browse the documentation online:

http://httpproxy.yvandermeer.net/


[1]: https://bitbucket.org/yvandermeer/django-http-proxy
[2]: https://github.com/yvandermeer/django-http-proxy
